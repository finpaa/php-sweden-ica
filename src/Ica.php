<?php

namespace Finpaa\Sweden;

use Finpaa\Finpaa; 

class Ica
{
    // Sequence Codes
    private static $redirectAuth;
    private static $ais;
    private static $pisSepa;
    private static $pisCrossBorder;

    private static function selfConstruct()
    {
        $finpaa = new Finpaa();

        $sequences = $finpaa->Sweden()->{env('FINPAA_ENVIRONMENT')}()->ICA_Updated();

        self::$redirectAuth = $sequences->ICA_Redirect_Auth();
        self::$ais = $sequences->ICA_AIS();
        self::$pisSepa = $sequences->ICA_PIS_SEPA();
        self::$pisCrossBorder = $sequences->ICA_PIS_CrossBorder();
    }

    private static function getSequenceCode($name) {
      if(self::$$name) {
        return self::$$name;
      }
      else {
        self::selfConstruct();
        return self::getSequenceCode($name);
      }
    }

    private static function executeSequenceMethod($code, $methodIndex, $alterations, $name, $returnPayload)
    {
        $sequence = Finpaa::getSequenceMethods($code);

        if (isset($sequence->SequenceExecutions)) {

          if($methodIndex < count($sequence->SequenceExecutions))
          {
            $response = Finpaa::executeTheMethod(
              $sequence->SequenceExecutions[$methodIndex]->methodCode, $alterations, $returnPayload
            );

            return array('error' => false, 'response' => $response);
          }
          else {
              return array('error' => true, 'message' => $name . ' method Failed -> Index out of bound',
                'methodIndex' => $methodIndex, 'methodsCount' => count($sequence->SequenceExecutions)
              );
          }
        }
        else {
           return array('error' => true, 'message' => $name .' -> No sequence executions', 'response' => $sequence);
       }
    }

    public static function redirectAuthorize($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('redirectAuth'), $methodIndex, $alterations, 'ICA Redirect Auth', $returnPayload
        );
    }
    public static function AIS($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('ais'), $methodIndex, $alterations, 'ICA AIS', $returnPayload
        );
    }
    public static function PisSepa($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('pisSepa'), $methodIndex, $alterations, 'ICA PIS SEPA', $returnPayload
        );
    }
    public static function PisCrossBorder($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('pisCrossBorder'), $methodIndex, $alterations, 'ICA PIS Cross Border', $returnPayload
        );
    }
}
